// STEPS 	
	// require http
let http = require("http")

const PORT = 3000;

	// method to create server
// http.createServer( (request, response) => {}).listen(PORT);
http.createServer( (request, response) => {
	// uri/endpoint = resource
	// http method
	// body
	console.log(request);
	if(request.url == "/profile" && request.method === "GET"){

		// logic

		// before sending the response
		response.writeHead(200,
			{"Content-Type": "text/plain"}
		);
		response.end("Welcome to my page.");
	} 
	else if(request.url === "/profile" && request.method === "POST"){
		response.writeHead( 200,
			{"Content-Type": "text/plain"});
		response.end("Data to be sent to the database")
	} else {
		response.writeHead (404,
			{"Content-Type": "text/plain"});
		response.end(`Sorry, the page is not found.`);
	}

	
}).listen(PORT);

console.log(`Server is now connected to port ${PORT}`);
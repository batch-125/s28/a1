let http = require("http")

const PORT = 3000;

let directory = [
	{
		"name": "Brandon",
		"email": "brandon@mail.com"
	},
	{
		"name": "Robert",
		"email": "robert@mail.com"
	}
];

http.createServer((req,res)=>{
	
	// retrieve our documents
	//url = /users
	// method = Get
	// response = array of objects

	if (req.url === "/users" && req.method == "GET"){
	res.writeHead(200, {"Content-Type": "application/json"});
	// console.log(directory);
	res.write(JSON.stringify(directory));
	res.end();
	} 

// create a new user
	// url = /users
	// method = Post
	// data from the request will be coming from the client
		// req.body

	if (req.url === "/users" && req.method === "POST"){

		let reqBody = "";
		req.on("data", (data) => {

			// console.log(data)
			reqBody += data;

		});  //has something to do with string continuously receiving data

		req.on("end", ()=> {
			console.log(typeof reqBody)

			reqBody = JSON.parse(reqBody); 	
			//parsed into a real javascript object
			console.log(reqBody)

		let newUser = {
			"name": reqBody.name,
			"email": reqBody.email
		}

		directory.push(newUser)
		console.log(directory)
			/*[
			  { name: 'Brandon', email: 'brandon@mail.com' },
			  { name: 'Robert', email: 'robert@mail.com' },
			  { name: 'Mash', email: 'mash@mail.com' }
			]*/
		res.writeHead( 200,{"Content-Type": "application/json"});
		res.write(JSON.stringify(directory));
		res.end();
		
		});
	}

}).listen(PORT);

console.log(`Server is now connected to port ${PORT}`)